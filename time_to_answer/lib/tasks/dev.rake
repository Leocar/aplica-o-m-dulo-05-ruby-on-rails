namespace :dev do

  DEFAULT_FILES_PATH = File.join(Rails.root,'lib','tmp')

  desc "TODO"
  task setup: :environment do
    puts %x(rake db:drop db:create db:migrate)
    puts "DataBase Rest Concluido"
    Admin.create!(email: 'adm@adm.com', password: 123456, password_confirmation: 123456)
    User.create!(email: 'test@test.com', password: 123456, password_confirmation: 123456)

    puts "Tudo certo até aqui"
   
  end
  desc 'Nada'
  task plus: :environment do
      a = 123456
      5.times do |i|
        Admin.create!(
          email: Faker::Internet.email,
          password: a,
          password_confirmation: a
        )
      end
  end 
  desc 'Cadastro de assuntos'
  task assuntos: :environment do
      file_name = 'subjects.txt'
      file_patch =  File.join(DEFAULT_FILES_PATH, file_name)

      File.open(file_patch,'r').each do |line|
        Subject.create!(description: line.strip)
      end
  end
  desc 'Cadastro de questões e respostas'
  task questions: :environment do
      Subject.all.each do |subject|
        rand(5..10).times do |i|
          Question.create!(
            description: "#{Faker::Lorem.paragraph}#{Faker::Lorem.question}",
            subject: subject
          )
        end
      end
  end

end
