module AdminsBackofficeHelper
    def translate_attribute(object = nil, method= nil)
        # (object && method ) ? object.model.human_attribute_name(method) : "Informe os parametros corretamente"
        #Ternario A cima
        
        if object and method
            object.model.human_attribute_name(method)
        else
            "Informe os parametros corretamente"
        end
    end
end
