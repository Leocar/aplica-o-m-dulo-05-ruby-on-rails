class AdminsBackoffice::QuestionsController < AdminsBackofficeController
    before_action :set_question, only: [:show,:edit,:update,:destroy]
    before_action :question_params, only: [:update, :create]
    before_action :set_type_question, only: [:edit,:new]
    def index
      @questions = Question.includes(:subject).all #Incluindo isso para evitar o n+1
    end
  
    def new
      @question = Question.new
    end
  
    def create
      @question = Question.new(question_params)
      if @question.save.inspect
        redirect_to admins_backoffice_questions_path , notice: "Assunto criado com sucesso" 
      else
        if @question.errors.any?
          @erros = @question.errors.full_messages
          render :new
        end
      end
    end
  
    def edit 
      params[:id]# passo o parametro :id na url 
      #@admin = Admin.find(params[:id])#Busco pelo o id informado no banco
    end
  
    def destroy
        @question.destroy
        redirect_to admins_backoffice_questions_path , notice: "Pergunta deletada com sucesso"
    end
  
    def update #Update
      #params_admin = params.require(:admin).permit(:email,:password,:password_confirmation)
      # Seto uma def que pegar o id private chamada admin_params e o parametro que passo pra ela é o hash :admin 
      #que está no form do edit.html
     
      if @question.update(question_params) # pego os dados  antigos da variavel admin e set os novos do form
        redirect_to admins_backoffice_questions_paths , notice: "Alterações efetuadas com sucesso" #redireciono para a pag 
      else
        if @question.errors.any?
          @erros = @question.errors.full_messages
          render :edit
        end
      end
    end
  
    private
  
    def set_question
      @question = Question.find(params[:id])
    end
  
    def question_params
      params.require(:question).permit(:description,:subject) #pego o hash do form com id informado
    end

    def set_type_question
      @set_type_question = Subject.all
    end
  
    
end


