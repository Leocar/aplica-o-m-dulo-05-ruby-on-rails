class AdminsBackoffice::AdminsController < AdminsBackofficeController
  before_action :set_adm, only: [:show,:edit,:update,:destroy]
  before_action :pass_verify, only: [:update]
  before_action :admin_params, only: [:update, :create]
  def index
    @admins = Admin.all
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(admin_params)
    if @admin.save.inspect
      redirect_to admins_backoffice_admins_path , notice: "Adm criado com sucesso" 
    else
      if @admin.errors.any?
        @erros = @admin.errors.full_messages
        render :new
      end
    end
  end

  def edit 
    params[:id]# passo o parametro :id na url 
    #@admin = Admin.find(params[:id])#Busco pelo o id informado no banco
  end

  def destroy
      @admin.destroy
      redirect_to admins_backoffice_admins_path , notice: "Deletado com sucesso"
  end

  def update #Update
    #params_admin = params.require(:admin).permit(:email,:password,:password_confirmation)
    # Seto uma def que pegar o id private chamada admin_params e o parametro que passo pra ela é o hash :admin 
    #que está no form do edit.html
   
    if @admin.update(admin_params) # pego os dados  antigos da variavel admin e set os novos do form
      redirect_to admins_backoffice_admins_path , notice: "Alterações efetuadas com sucesso" #redireciono para a pag 
    else
      if @admin.errors.any?
        @erros = @admin.errors.full_messages
        render :edit
      end
    end
  end

  private

  def set_adm
    @admin = Admin.find(params[:id])
  end

  def admin_params
    params.require(:admin).permit(:email,:password,:password_confirmation) #pego o hash do form com id informado
  end

  def pass_verify
    if params[:admin][:password].blank? and params[:admin][:password_confirmation].blank?
      params[:admin].extract!(:password,:password_confirmation)
    end
  end

end
