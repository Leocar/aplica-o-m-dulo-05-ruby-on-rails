class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.text :description, null: false
      t.references :question, index: true, foreign_key: true
      t.boolean :correct, default: false

      t.timestamps null: false
    end
  end
end
