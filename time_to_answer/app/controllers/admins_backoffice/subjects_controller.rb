class AdminsBackoffice::SubjectsController < AdminsBackofficeController
    before_action :set_sub, only: [:show,:edit,:update,:destroy]
    before_action :sub_params, only: [:update, :create]
    def index
      @subs = Subject.all
    end
  
    def new
      @sub = Subject.new
    end
  
    def create
      @sub = Subject.new(sub_params)
      if @sub.save.inspect
        redirect_to admins_backoffice_subjects_path , notice: "Assunto criado com sucesso" 
      else
        if @sub.errors.any?
          @erros = @sub.errors.full_messages
          render :new
        end
      end
    end
  
    def edit 
      params[:id]# passo o parametro :id na url 
      #@admin = Admin.find(params[:id])#Busco pelo o id informado no banco
    end
  
    def destroy
        @sub.destroy
        redirect_to admins_backoffice_subjects_path , notice: "Pergunta deletada com sucesso"
    end
  
    def update #Update
      #params_admin = params.require(:admin).permit(:email,:password,:password_confirmation)
      # Seto uma def que pegar o id private chamada admin_params e o parametro que passo pra ela é o hash :admin 
      #que está no form do edit.html
     
      if @sub.update(sub_params) # pego os dados  antigos da variavel admin e set os novos do form
        redirect_to admins_backoffice_subjects_path , notice: "Alterações efetuadas com sucesso" #redireciono para a pag 
      else
        if @sub.errors.any?
          @erros = @sub.errors.full_messages
          render :edit
        end
      end
    end
  
    private
  
    def set_sub
      @sub = Subject.find(params[:id])
    end
  
    def sub_params
      params.require(:sub).permit(:description) #pego o hash do form com id informado
    end
  
    
end
