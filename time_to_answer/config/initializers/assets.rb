# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( admins_backoffice.js admins_backoffice.css )
Rails.application.config.assets.precompile += %w( users_backoffice.js users_backoffice.css )
Rails.application.config.assets.precompile += %w( sb-admin-2.css sb-admin-2.js img.jpg)
Rails.application.config.assets.precompile += %w( custom.min.css custom.min.js custom.css custom.js img.jpg)
Rails.application.config.assets.precompile += %w( jquery-2.2.3/dist/jquery.js jquery-2.2.3/dist/jquery.min.js )



