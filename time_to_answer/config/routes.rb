Rails.application.routes.draw do
  namespace :admins_backoffice do #List
  #get 'admins/index', only:[:index,:edit,:update,:destroy]
  #get 'admins/edit/:id', to: 'admins#edit' adição rota a mao 
  resources :admins
  resources :subjects
  resources :questions
  end

  namespace :users_backoffice do
  get 'welcome/index'
  resources :users
  end

  
  namespace :site do
  get 'welcome/index'
  end

  namespace :users_backoffice do
    get 'welcome/index'
  end
  devise_for :users

  namespace :admins_backoffice do
    get 'welcome/index' #inicio
  end

  devise_for :admins

  get 'inicio', to: 'site/welcome#index'
  
  root to: 'site/welcome#index' # Defiinindo rota padrão 

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
